namespace VinjectDemo{

    /**
    * This is simply a way to place every service token in one place so they are easily reachable
    **/
    public class Services : Object{
        
        public static Vinject.ServiceToken<IAWCore> aw_core = new Vinject.ServiceToken<IAWCore>();
        public static Vinject.ServiceToken<DesktopLayout> layout = new Vinject.ServiceToken<DesktopLayout>();
        public static Vinject.ServiceToken<string> reverse_domain_name = new Vinject.ServiceToken<string>();
        //  public static Vinject.ServiceToken<GLib.ApplicationFlags> app_flags = new Vinject.ServiceToken<GLib.ApplicationFlags>();
        
        //  public static Vinject.ServiceToken<Gtk.Application> application = new Vinject.ServiceToken<Gtk.Application>();
        //  public static Vinject.ServiceToken<Gtk.ApplicationWindow> app_window = new Vinject.ServiceToken<Gtk.ApplicationWindow>();

        public static Vinject.Injector app_container(){
            var injector = new Vinject.Injector();
    
            injector.register_constant(reverse_domain_name, "com.gitlab.geeky-endeavours.vinject-demo");
            //  injector.register_constant(app_flags, GLib.ApplicationFlags.FLAGS_NONE);

            injector.register_resolution<AWCore, IAWCore>(aw_core);
            //  injector.register_singleton<Gtk.Application, Gtk.Application>(application, application_id: reverse_domain_name, flags: app_flags);

            //  injector.register_transient<Gtk.ApplicationWindow, Gtk.ApplicationWindow>(app_window, child: layout);

            return injector;
        }
    }

    public interface IAWCore : Object{
        public abstract int id { get; }
    }

    public class AWCore : Object, IAWCore {

        protected static int counter = 0;
        private int _id;
        public int id { get { return this._id; } }

        construct{
            this._id = counter++;
        }

    }

    public class DesktopLayout : Gtk.Box {

        public IAWCore aw_core { get; construct; }
        public Gtk.Button start_button { get; construct; }
        public Gtk.Label aw_core_id_label { get; construct; }

        construct{
            this.aw_core_id_label = new Gtk.Label("");
            this.start_button = new Gtk.Button.with_label("Show aw_core Id");

            this.start_button.clicked.connect(() => {
                this.aw_core_id_label.set_text(this.aw_core.id.to_string());
            });

            base.set_orientation(Gtk.Orientation.VERTICAL);
            base.append(this.aw_core_id_label);
            base.append(this.start_button);
        }

    }

    

    public static int main(string[] argv){

        Vinject.ServiceToken<IAWCore> aw_core = new Vinject.ServiceToken<IAWCore>();
        Vinject.ServiceToken<DesktopLayout> layout = new Vinject.ServiceToken<DesktopLayout>();
        Vinject.ServiceToken<string> reverse_domain_name = new Vinject.ServiceToken<string>();
        Vinject.ServiceToken<GLib.ApplicationFlags> app_flags = new Vinject.ServiceToken<GLib.ApplicationFlags>();
        
        Vinject.ServiceToken<Gtk.Application> application = new Vinject.ServiceToken<Gtk.Application>();
        Vinject.ServiceToken<Gtk.ApplicationWindow> app_window = new Vinject.ServiceToken<Gtk.ApplicationWindow>();

        var injector = new Vinject.Injector();
    
        injector.register_constant(reverse_domain_name, "com.gitlab.geeky-endeavours.vinject-demo");
        //  injector.register_constant(app_flags, GLib.ApplicationFlags.FLAGS_NONE);

        injector.register_resolution<AWCore, IAWCore>(aw_core);
        injector.register_transient<DesktopLayout, Gtk.Widget>(layout);
        //  injector.register_singleton<Gtk.Application, Gtk.Application>(application, application_id: reverse_domain_name, flags: app_flags);

        //  injector.register_transient<Gtk.ApplicationWindow, Gtk.ApplicationWindow>(app_window, child: layout);

        var app = new Gtk.Application(injector.obtain(reverse_domain_name), GLib.ApplicationFlags.FLAGS_NONE); //container.obtain(Services.application);
        app.activate.connect(() => {
            var window = new Gtk.ApplicationWindow(app); //container.obtain(Services.app_window);
            window.set_application(app);

            window.set_child(injector.obtain(layout));
            window.present();
        });

        message("hullo");
        return app.run(argv);
    }

}