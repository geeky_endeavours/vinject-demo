# vinject-demo

Currently this is only in a semi working state. 

There are a few things (as can be seen in the code) that does not behave quite like I want it to.

The general Idea is here though, demonstrated in working fashion. Currently Vinject cannot build a `Gtk.Application`, because it requires injecting the `GLib.ApplicationFlags`, and Vinject does not yet handle enums properly.